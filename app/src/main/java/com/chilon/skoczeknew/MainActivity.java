package com.chilon.skoczeknew;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.chilon.skoczeknew.settings.language.LanguageActivity;
import com.chilon.skoczeknew.settings.login.LoginFragment;
import com.chilon.skoczeknew.settings.login.LoginOrRegisterFragment;
import com.chilon.skoczeknew.settings.login.RegisterFragment;
import com.chilon.skoczeknew.settings.manual.ManualFragment;
import com.chilon.skoczeknew.settings.ranking.RankingFragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private ImageView home;
    private FirebaseAuth mAuth;
    InterstitialAd rankingAd;
    FirebaseFunctions mFunctions;
    FirebaseUser fireBaseUser;

    NavigationView navigationView;
    public static HashMap<String, String> gameStatus = new HashMap<>();
    TextView abUser, abGlobalScore, abCountryCode, abCountryScore, abCity, abCityScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_main);

        if (gameStatus.size() == 0) {
            initGameStatus();
        }
        if (hasLocationPermission()) {
            getGpsLocation();
        }

        rankingAd = new InterstitialAd(this);
        rankingAd.setAdUnitId(getResources().getString(R.string.ad_full_scr_fb_connection));
        rankingAd.loadAd(new AdRequest.Builder().build());

        mAuth = FirebaseAuth.getInstance();
        fireBaseUser = mAuth.getCurrentUser();
        mFunctions = FirebaseFunctions.getInstance();

        View view = findViewById(R.id.toolbar_layout);
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = getScreenWidthInPixels() - getActionBarHeightInPixels()*2;;
        view.setLayoutParams(params);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appBarRanking();

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        home = findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyGlobals.FRIENDS_SECTION_ACTIVE = false;
                navigateToChooseBoardFragment();
            }
        });

        Menu navMenu = navigationView.getMenu();

        if (fireBaseUser != null) {
            navMenu.findItem(R.id.login_user).setVisible(false);
            navMenu.findItem(R.id.register_user).setVisible(false);
            updateGameStatus();
        }

        getSupportActionBar().setTitle("");

    }

    public void appBarRanking(){

        abUser = findViewById(R.id.abUser);
        abGlobalScore = findViewById(R.id.abGlobalScore);
        abCountryCode = findViewById(R.id.abCountryCode);
        abCountryScore = findViewById(R.id.abCountryScore);
        abCity = findViewById(R.id.abCity);
        abCityScore = findViewById(R.id.abCityScore);

        abUser.setText(loadSharedPrefs(MyGlobals.USER_NAME_KEY));
        abGlobalScore.setText(loadSharedPrefs(MyGlobals.GLOBAL_POSITION_KEY));
        abCountryScore.setText(loadSharedPrefs(MyGlobals.COUNTRY_POSITION_KEY));
        abCountryCode.setText(loadSharedPrefs(MyGlobals.COUNTRY_KEY));
        abCity.setText(loadSharedPrefs(MyGlobals.CITY_KEY));
        abCityScore.setText(loadSharedPrefs(MyGlobals.CITY_POSITION_KEY));


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getGpsLocation();
    }

    private boolean hasLocationPermission() {
        //Check if the user has not granted permission...
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Prompt the user to grant permission...
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            return false;
        }
        //Return true if the permission is already granted...
        return true;
    }

    private void initGameStatus() {
        gameStatus.put("5 x 5", "0");
        gameStatus.put("5 x 6", "0");
        gameStatus.put("5 x 7", "0");
        gameStatus.put("5 x 8", "0");
        gameStatus.put("5 x 9", "0");
        gameStatus.put("6 x 6", "0");
        gameStatus.put("6 x 7", "0");
        gameStatus.put("6 x 8", "0");
        gameStatus.put("6 x 9", "0");
        gameStatus.put("7 x 7", "0");
        gameStatus.put("7 x 8", "0");
        gameStatus.put("7 x 9", "0");
        gameStatus.put("8 x 8", "0");
        gameStatus.put("8 x 9", "0");
        gameStatus.put("9 x 9", "0");
    }

    private void updateGameStatus() {
        Intent intent = getIntent();

        for (Map.Entry<String, String> entry : gameStatus.entrySet()) {
            if (intent.hasExtra(entry.getKey())) {
                if (Integer.parseInt(intent.getStringExtra(entry.getKey())) > Integer.parseInt(loadSharedPrefs(entry.getKey()))) {
                    saveInSharedPrefs(entry.getKey(), intent.getStringExtra(entry.getKey()));
                }
            }
        }
        if (intent.hasExtra("city") && !intent.getStringExtra("city").equals("0")){
            saveInSharedPrefs(MyGlobals.CITY_KEY,intent.getStringExtra("city"));
        }
        if (intent.hasExtra("country") && !intent.getStringExtra("country").equals("0")){
            saveInSharedPrefs(MyGlobals.COUNTRY_KEY,intent.getStringExtra("country"));
        }

    }

    public void getGpsLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            final LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setInterval(60000);
            locationRequest.setFastestInterval(5000);
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            LocationServices.getFusedLocationProviderClient(getApplicationContext()).requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    LocationServices.getFusedLocationProviderClient(getApplicationContext()).removeLocationUpdates(this);
                    if (locationResult != null && locationResult.getLocations().size() > 0) {
                        int lastLocationIndex = locationResult.getLocations().size() - 1;
                        double latitude = locationResult.getLocations().get(lastLocationIndex).getLatitude();
                        double longitude = locationResult.getLocations().get(lastLocationIndex).getLongitude();

                        getCityAndCountry(latitude, longitude);
                    }
                }
            }, Looper.getMainLooper());

        }
    }

    private void getCityAndCountry(double lat, double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(lat, lon, 3);
            if (loadSharedPrefs(MyGlobals.CITY_KEY).equals("0")) {
                saveInSharedPrefs(MyGlobals.CITY_KEY, addresses.get(0).getLocality());
            }
            if (loadSharedPrefs(MyGlobals.COUNTRY_KEY).equals("0")) {
                saveInSharedPrefs(MyGlobals.COUNTRY_KEY, addresses.get(0).getCountryName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (fireBaseUser != null) {
            navigateToChooseBoardFragment();
        } else {
            navigateToLoginOrRegisterFragment();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.change_language:
                startActivity(new Intent(getApplicationContext(), LanguageActivity.class));
                break;

            case R.id.register_user:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RegisterFragment()).commit();
                break;

            case R.id.login_user:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LoginFragment()).commit();
                break;

            case R.id.ranking:
                if (rankingAd.isLoaded()){
                    rankingAd.show();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RankingFragment()).commit();
                break;

            case R.id.rate_app:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.chilon.skoczeknew")));
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id=com.chilon.skoczeknew")));
                }
                break;

//            case R.id.donate:
//                break;
//
//            case R.id.remove_ads:
//                break;

            case R.id.invite_friends:
                shareApp();
                break;

            case R.id.manual:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ManualFragment()).commit();
                break;
        }
        drawer.closeDrawers();
        return true;
    }

    public void shareApp(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String shareAppLink = "https://play.google.com/store/apps/details?id=com.chilon.skoczeknew";
        intent.putExtra(Intent.EXTRA_TEXT,shareAppLink);
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.app_name)));
    }

    public void navigateToChooseBoardFragment() {
        appBarRanking();

        if (loadSharedPrefs(MyGlobals.SHOW_MANUAL_BY_EVERY_START).equals("0")){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container,new ManualFragment())
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, new ChooseBoardFragment())
                    .commit();
        }





    }

    private void navigateToLoginOrRegisterFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new LoginOrRegisterFragment())
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            navigateToChooseBoardFragment();
        }
    }

    public void loadLocale() {
        SharedPreferences prefs = getSharedPreferences(MyGlobals.SHARED_PREFS_POOL, Activity.MODE_PRIVATE);
        String language = prefs.getString("My_Lang", "");
        if (language.equals("")) {
            Locale.getDefault().getDisplayLanguage();
        } else {
            setLanguageForApp(language);
        }
    }

    public int getScreenWidthInPixels() {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public int getActionBarHeightInPixels(){
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }

    public void setLanguageForApp(String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
    }

    public String loadSharedPrefs(String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(MyGlobals.SHARED_PREFS_POOL, MODE_PRIVATE);
        return sharedPreferences.getString(key, "0");
    }

    public void saveInSharedPrefs(String key, String value) {
        SharedPreferences sharedPreferences = getSharedPreferences(MyGlobals.SHARED_PREFS_POOL, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

}