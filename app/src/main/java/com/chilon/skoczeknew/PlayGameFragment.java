package com.chilon.skoczeknew;

import android.animation.Animator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.gridlayout.widget.GridLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.Locale;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.WINDOW_SERVICE;

public class PlayGameFragment extends Fragment implements View.OnClickListener {

    public static final int COLOR_MOVE_POSSIBLE = Color.MAGENTA;
    public static final int COLOR_CLICKED = Color.GREEN;
    public static final int COLOR_CLICKED_TEMP = Color.parseColor("#fff400");
    public static final int COLOR_NOT_CLICKED = Color.BLUE;
    public static final String SHARED_PREFS_POOL = "anySizePrefs";

    GridLayout gridLayoutAnySize;
    TextView result, scoreForBestResult, textForBestResult, scoreTimePoints, scoreTotal;
    int currentResult = 0;
    int bestResultEver = 0;
    int column;
    int row;
    String currentBoard_KEY;
    TextView resetBoardTextView;
    int newRecordSignalLimiter = 0;
    LinearLayout linearLayoutBestResult;
    ProgressBar mProgressBar;
    CountDownTimer mCountDownTimer;
    int timePB;
    int intervalPB;
    int positionPB;
    int timePoints;
    boolean countDownIsRunning;
    boolean noMorePossibleMoves;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_play_game, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        linearLayoutBestResult = view.findViewById(R.id.layoutBestResult);
        result = view.findViewById(R.id.textBestCurrentResultAnySize);
        scoreForBestResult = view.findViewById(R.id.scoreForBestResultEverAnySize);
        textForBestResult = view.findViewById(R.id.textForBestScore);
        mProgressBar = view.findViewById(R.id.progress_bar_time);
        scoreTimePoints = view.findViewById(R.id.score_time_points);
        scoreTotal = view.findViewById(R.id.score_total_points);

        resetBoardTextView = view.findViewById(R.id.textViewResetBoard);
        resetBoardTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSharedPrefs();
                currentResult = 0;
                result.setText("0");
                linearLayoutBestResult.setBackgroundColor(getResources().getColor(R.color.background_four_text));
                scoreForBestResult.setText(Integer.toString(bestResultEver));
                //textForBestResult.setText("Najlepszy wynik: ");
                gridLayoutAnySize.removeAllViews();
                createGridLayout(gridLayoutAnySize, view);
                loadSharedPrefs();
                scoreForBestResult.setText(Integer.toString(bestResultEver));
                newRecordSignalLimiter = 0;
                scoreTimePoints.setText(String.valueOf(0));
                scoreTotal.setText(String.valueOf(0));
                progressBarCancel();
            }
        });

        scoreTotal.setText(String.valueOf(timePoints));

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            column = bundle.getInt("columns");
            row = bundle.getInt("rows");
            currentBoard_KEY = Integer.toString(column) + " x " + Integer.toString(row);
        }
        loadSharedPrefs();
        scoreForBestResult.setText(Integer.toString(bestResultEver));
        gridLayoutAnySize = view.findViewById(R.id.gridLayoutAnySize);
        gridLayoutAnySize.removeAllViews();
        createGridLayout(gridLayoutAnySize, view);

    }

    public void progressBarCancel() {
        if (countDownIsRunning) {
            mCountDownTimer.cancel();
        }
        mProgressBar.setVisibility(View.GONE);
    }

    public void progressBarPause() {
        if (countDownIsRunning) {
            mCountDownTimer.cancel();
        }
        mProgressBar.setProgress(positionPB);
        //createAlert( scoresInfo());
    }

    public String scoresInfo(int messageNo) {

        if (messageNo == 1) { //if (currentResult==row*column && timePoints>0)
            return  getResources().getString(R.string.pgf_time_points) + timePoints +". "+ getResources().getString(R.string.pgf_ordinary_points) + currentResult + ". ";
        }
        if (messageNo == 2) { //if (currentResult==row*column && timePoints==0)
            return  getResources().getString(R.string.pgf_board_compl_no_time) + currentResult + getResources().getString(R.string.pgf_board_compl_be_faster);
        } else {
            return  getResources().getString(R.string.pgf_ordinary_points) + currentResult + getResources().getString(R.string.pgf_suggestion);
        }
}

    public void createAlert(String message) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setIcon(R.drawable.skoczek_transparent);
        final AlertDialog alert = dialog.create();
        alert.show();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                progressBarCancel();
            }
        });
    }

    public void createGridLayout(GridLayout myGrid, View view) {
        myGrid.setColumnCount(column);
        myGrid.setRowCount(row);

        for (int x = 1; x < row + 1; x++) {
            for (int y = 1; y < column + 1; y++) {
                TextView textView = new TextView(this.getActivity());
                android.widget.GridLayout.LayoutParams param = new android.widget.GridLayout.LayoutParams();
                int margin = 3;
                int height = (getPlayGameAreaHeightInPixels() / row) - margin - 3;
                int width = (getScreenWidthInPixels() / column) - margin - 3;

                if (y == 1) {
                    param.leftMargin = margin;
                }
                if (x == 1) {
                    param.topMargin = margin;
                }

                param.rightMargin = margin;
                param.bottomMargin = margin;
                param.width = width;
                param.height = height;
                param.setGravity(Gravity.CENTER);
                param.columnSpec = android.widget.GridLayout.spec(y);
                param.rowSpec = android.widget.GridLayout.spec(x);

                textView.setLayoutParams(param);
                textView.setBackgroundColor(COLOR_NOT_CLICKED);
                textView.setGravity(Gravity.CENTER);
                textView.setOnClickListener(this);
                String id = Integer.toString(x) + Integer.toString(y);
                textView.setId(Integer.parseInt(id));

                myGrid.addView(textView);
            }
        }
    }

    @Override
    public void onClick(View v) {
        TextView field = (TextView) getView().findViewById(getResources().getIdentifier(Integer.toString(v.getId()), "id", getActivity().getPackageName()));
        ColorDrawable itemColor = (ColorDrawable) v.getBackground();
        String id = String.valueOf(v.getId());
        int posX = Integer.parseInt(id.substring(0, 1));
        int posY = Integer.parseInt(id.substring(1, 2));

        if (itemColor.getColor() == COLOR_NOT_CLICKED && currentResult == 0) {
            showPossibleMoves(posX, posY, v);
            field.setBackgroundColor(COLOR_CLICKED_TEMP);
            currentResult++;
            field.setText(Integer.toString(currentResult));
            result.setText(Integer.toString(currentResult));
            progressBarRunTime();
        }
        if (itemColor.getColor() == COLOR_MOVE_POSSIBLE) {
            cancelOldMovesPossible();
            field.setBackgroundColor(COLOR_CLICKED_TEMP);
            currentResult++;
            field.setText(Integer.toString(currentResult));
            showPossibleMoves(posX, posY, v);
            result.setText(String.format(Locale.getDefault(), "%d", currentResult));
            if (currentResult > bestResultEver && newRecordSignalLimiter == 0) {
                newRecordSignal();
            }
            if (currentResult == row * column && countDownIsRunning) {
                createAlert(scoresInfo(1));
                progressBarPause();
                currentResult = currentResult + timePoints;
                scoreTotal.setText(String.valueOf(currentResult));
                saveSharedPrefs();
                if (currentResult>bestResultEver){
                    newRecordSignal();
                }
            }
            if (noMorePossibleMoves && currentResult<row*column) {
                createAlert(scoresInfo(3));
                progressBarPause();
            }
            if (currentResult==row*column && !countDownIsRunning){
                createAlert(scoresInfo(2));
                progressBarPause();
            }
        }
    }

    public void progressBarRunTime() {
        mProgressBar.setVisibility(View.VISIBLE);
        timePB = column * row * 1000;
        intervalPB = 30;
        mProgressBar.setMax(timePB);
        mCountDownTimer = new CountDownTimer(timePB, intervalPB) {
            @Override
            public void onTick(long millisUntilFinished) {
                positionPB = (int) Math.round(timePB - millisUntilFinished);
                mProgressBar.setProgress(positionPB);
                timePoints = Math.round(millisUntilFinished) / 100;
                scoreTimePoints.setText(String.valueOf(timePoints));
                countDownIsRunning = true;
            }

            @Override
            public void onFinish() {
                mProgressBar.setVisibility(View.GONE);
                countDownIsRunning = false;
            }
        };
        mCountDownTimer.start();
    }

    private void newRecordSignal() {
        linearLayoutBestResult.setBackground(getResources().getDrawable(R.drawable.button_border_new_record, null));
        textForBestResult.setText("New Record!!!");
        scoreForBestResult.setText("");

        YoYo.with(Techniques.Flash)
                .duration(1500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                })
                .playOn(linearLayoutBestResult);
        newRecordSignalLimiter = 1;
    }

    public void cancelOldMovesPossible() {

        for (int i = 0; i < gridLayoutAnySize.getChildCount(); i++) {
            View child = gridLayoutAnySize.getChildAt(i);
            ColorDrawable childBackground = (ColorDrawable) child.getBackground();
            if (childBackground.getColor() == COLOR_CLICKED_TEMP) {
                child.setBackgroundColor(COLOR_CLICKED);
            }
            if (childBackground.getColor() != COLOR_CLICKED) {
                child.setBackgroundColor(COLOR_NOT_CLICKED);
            }
        }
    }

    public void showPossibleMoves(int posX, int posY, View v) {
        noMorePossibleMoves = true;
        if (posX + 1 <= row && posY + 2 <= column) {
            setColorPossibleMove(posX + 1, posY + 2, v);
        }
        if (posX + 2 <= row && posY + 1 <= column) {
            setColorPossibleMove(posX + 2, posY + 1, v);
        }
        if (posX - 1 >= 1 && posY + 2 <= column) {
            setColorPossibleMove(posX - 1, posY + 2, v);
        }
        if (posX - 2 >= 1 && posY + 1 <= column) {
            setColorPossibleMove(posX - 2, posY + 1, v);
        }
        if (posX + 1 <= row && posY - 2 >= 1) {
            setColorPossibleMove(posX + 1, posY - 2, v);
        }
        if (posX + 2 <= row && posY - 1 >= 1) {
            setColorPossibleMove(posX + 2, posY - 1, v);
        }
        if (posX - 1 >= 1 && posY - 2 >= 1) {
            setColorPossibleMove(posX - 1, posY - 2, v);
        }
        if (posX - 2 >= 1 && posY - 1 >= 1) {
            setColorPossibleMove(posX - 2, posY - 1, v);
        }
    }

    public void setColorPossibleMove(int x, int y, View v) {
        String idX = Integer.toString(x);
        String idY = Integer.toString(y);
        String idXY = idX + idY;
        TextView field = (TextView) getView().findViewById(getResources().getIdentifier(idXY, "id", Objects.requireNonNull(getActivity()).getPackageName()));
        ColorDrawable fieldColor = (ColorDrawable) field.getBackground();
        if (fieldColor.getColor() != COLOR_CLICKED) {
            field.setBackgroundColor(COLOR_MOVE_POSSIBLE);
            noMorePossibleMoves = false;
        }
    }

    public int getScreenWidthInPixels() {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) Objects.requireNonNull(getActivity()).getApplicationContext().getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public int getScreenHeightInPixels() {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) Objects.requireNonNull(getActivity()).getApplicationContext().getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public int getPlayGameAreaHeightInPixels() {

        return getScreenHeightInPixels() - getToolBarHeight() - convertDpToPixels(100) - 56;
    }

    public int convertDpToPixels(int value) {
        final float scale = getResources().getDisplayMetrics().density;

        return (int) (value * scale + 0.5f);
    }

    public int getToolBarHeight() {
        int[] attrs = new int[]{R.attr.actionBarSize};
        TypedArray ta = getContext().obtainStyledAttributes(attrs);
        int toolBarHeight = ta.getDimensionPixelSize(0, -1);
        ta.recycle();
        return toolBarHeight;
    }

    @Override
    public void onPause() {
        super.onPause();
        saveSharedPrefs();
        if (countDownIsRunning) {
            mCountDownTimer.cancel();
        }

    }

    public void saveSharedPrefs() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(SHARED_PREFS_POOL, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (bestResultEver < currentResult) {
            editor.putString(currentBoard_KEY, Integer.toString(currentResult));
        }
        editor.apply();
    }

    public void loadSharedPrefs() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(SHARED_PREFS_POOL, MODE_PRIVATE);
        bestResultEver = Integer.parseInt(sharedPreferences.getString(currentBoard_KEY, "0"));
    }
}
