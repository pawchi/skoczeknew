package com.chilon.skoczeknew;

import com.chilon.skoczeknew.settings.ranking.FriendItem;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class MyGlobals {

    public static final String USER_NAME_KEY = "userName";

    public static final String SHARED_PREFS_POOL = "anySizePrefs";
    public static final String TOTAL_RANKING_KEY = "totalRankKey";
    public static final String CITY_KEY = "city";
    public static final String CITY_POSITION_KEY = "city_position";
    public static final String COUNTRY_KEY = "country";
    public static final String COUNTRY_POSITION_KEY = "country_position";
    public static final String GLOBAL_POSITION_KEY = "global_position";
    public static final String FRIENDS_LIST_KEY = "friends_list";
    public static boolean FRIENDS_SECTION_ACTIVE = false;
    public static final String SHOW_MANUAL_BY_EVERY_START = "manual";
    public static boolean MANUAL_NOT_YET_WATCHED = true;

    public static boolean isEmailValid(String email){
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String decodeSpecialCharacters(String input) {
        return input.replace("."," ").replace("#"," ").replace("$"," ").replace("["," ").replace("]"," ");

        //return URLDecoder.decode(input,"utf-8");
    }

    public static String encodeSpecialCharacters(String input) throws UnsupportedEncodingException {
        return URLEncoder.encode(input,"utf-8");
    }

    public static boolean stringContainsSpecialCharacter(String input){
        return input.contains(".") || input.contains("#") || input.contains("$") || input.contains("[") || input.contains("]");
    }
}
