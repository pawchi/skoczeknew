package com.chilon.skoczeknew;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.chilon.skoczeknew.settings.manual.ManualFragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.WINDOW_SERVICE;

public class ChooseBoardFragment extends Fragment {

    GridLayout menuGridLayout;
    int screenWidth;
    int screenHeight;
    InterstitialAd adEvery4Min;
    public static final String SHARED_PREFS_POOL = "anySizePrefs";
    public static ArrayList<BoardInfo> boardInfosList = new ArrayList<>(Arrays.asList(new BoardInfo("5 x 5", "25"),
            new BoardInfo("5 x 6", "30"), new BoardInfo("5 x 7", "35"),
            new BoardInfo("5 x 8", "40"), new BoardInfo("5 x 9", "45"),
            new BoardInfo("6 x 6", "36"), new BoardInfo("6 x 7", "42"),
            new BoardInfo("6 x 8", "48"), new BoardInfo("6 x 9", "54"),
            new BoardInfo("7 x 7", "49"), new BoardInfo("7 x 8", "56"),
            new BoardInfo("7 x 9", "63"), new BoardInfo("8 x 8", "64"),
            new BoardInfo("8 x 9", "72"), new BoardInfo("9 x 9", "81")));
    private ArrayList<String> colors = new ArrayList<>(Arrays.asList("#05ffa1","#01cdfe","#fffb96","#ff71ce","#ff4765"));

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        adEvery4Min = new InterstitialAd(getContext());
        adEvery4Min.setAdUnitId(getResources().getString(R.string.ad_full_scr_4min));
        //adEvery4Min.setAdUnitId(getResources().getString(R.string.Ad_full_screen_test));
        adEvery4Min.loadAd(new AdRequest.Builder().build());

        return inflater.inflate(R.layout.fragment_choose_board, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        menuGridLayout = view.findViewById(R.id.startPageGridLayout);
        screenWidth = getScreenWidthInPixels();
        screenHeight = getScreenHeightInPixels();

        createMenuItems();
    }

    private void createMenuItems() {
        int loopPos = 1;
        int itemMarginWidth = 30;
        int screenMarginWidth = 20;
        int itemWidth = ((screenWidth - screenMarginWidth) / 3) - itemMarginWidth;
        int itemHeight = (screenHeight / 5) - 70;


        for (final BoardInfo boardInfo : boardInfosList) {
            LinearLayout linearLayout = new LinearLayout(this.getActivity());
            LinearLayout.LayoutParams scoreLinLayParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            scoreLinLayParam.gravity = Gravity.CENTER;
            linearLayout.setLayoutParams(scoreLinLayParam);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            TextView bestRes = new TextView(this.getActivity());
            bestRes.setText(getResources().getString(R.string.cbf__best_score));
            bestRes.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            TextView score = new TextView(this.getActivity());
            score.setText(loadSharedPrefs(boardInfo.getSize()));
            score.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            TextView numOfFields = new TextView(this.getActivity());
            String tempText = "/" + boardInfo.getNumberOfFields();
            numOfFields.setText(tempText);
            numOfFields.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            linearLayout.addView(bestRes);
            linearLayout.addView(score);
            //linearLayout.addView(numOfFields);

            LinearLayout mainLinLay = new LinearLayout(this.getActivity());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(itemWidth, itemHeight);
            params.gravity = Gravity.CENTER;
            mainLinLay.setGravity(Gravity.CENTER);
            params.setMargins(10, 10, 10, 10);
            mainLinLay.setOrientation(LinearLayout.VERTICAL);
            mainLinLay.setWeightSum(1);

            mainLinLay.setBackgroundResource(R.drawable.corners_bg);

            if (loopPos <= 5) {
                mainLinLay.getBackground().setTint(Color.parseColor(colors.get(0)));
            }
            if (loopPos >5 && loopPos<=9){
                mainLinLay.getBackground().setTint(Color.parseColor(colors.get(1)));
            }
            if (loopPos >9 && loopPos<=12){
                mainLinLay.getBackground().setTint(Color.parseColor(colors.get(2)));
            }
            if (loopPos >12 && loopPos<=14){
                mainLinLay.getBackground().setTint(Color.parseColor(colors.get(3)));
            }
            if (loopPos ==15){
                mainLinLay.getBackground().setTint(Color.parseColor(colors.get(4)));
            }
            loopPos++;

            mainLinLay.setLayoutParams(params);

            TextView boardSize = new TextView(this.getActivity());
            //if board completed
            if (Integer.parseInt(boardInfo.getNumberOfFields())<=Integer.parseInt(score.getText().toString())){
                //mainLinLay.getBackground().setTint(Color.parseColor(colors.get(4)));
                bestRes.setTextColor(ContextCompat.getColor(getContext(),R.color.text_board_completed_size));
                score.setTextColor(ContextCompat.getColor(getContext(),R.color.text_board_completed_size));
                numOfFields.setTextColor(ContextCompat.getColor(getContext(),R.color.text_board_completed_size));
                bestRes.setTypeface(null, Typeface.BOLD);
                score.setTypeface(null, Typeface.BOLD);
                numOfFields.setTypeface(null, Typeface.BOLD);
                boardSize.setTextColor(ContextCompat.getColor(getContext(),R.color.text_board_completed_size));
                mainLinLay.setBackground(getResources().getDrawable(R.drawable.shape_board_completed,null));

            } else {
                bestRes.setTextColor(ContextCompat.getColor(getContext(),R.color.text_black));
                score.setTextColor(ContextCompat.getColor(getContext(),R.color.text_black));
                numOfFields.setTextColor(ContextCompat.getColor(getContext(),R.color.text_black));
                boardSize.setTextColor(ContextCompat.getColor(getContext(),R.color.text_black));
            }
            boardSize.setText(boardInfo.getSize());
            boardSize.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            boardSize.setTextSize(35);

            mainLinLay.addView(linearLayout);
            mainLinLay.addView(boardSize);
            mainLinLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adEvery4Min.isLoaded()){
                        adEvery4Min.show();
                    }
                    Bundle bundle = new Bundle();
                    bundle.putInt("columns", Integer.parseInt(String.valueOf(boardInfo.getSize().charAt(0))));
                    bundle.putInt("rows", Integer.parseInt(boardInfo.getSize().substring(boardInfo.getSize().length() - 1)));
                    navigateToChoosenBoard(bundle);
                }
            });

            menuGridLayout.addView(mainLinLay);
        }
    }

    private void navigateToChoosenBoard(Bundle bundle) {
        PlayGameFragment playGameFragment = new PlayGameFragment();
        playGameFragment.setArguments(bundle);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, playGameFragment)
                .commit();
    }

    public int getScreenWidthInPixels() {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) Objects.requireNonNull(getActivity()).getApplicationContext().getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public int getScreenHeightInPixels() {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) Objects.requireNonNull(getActivity()).getApplicationContext().getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public String loadSharedPrefs(String key) {
        SharedPreferences sharedPreferences = Objects.requireNonNull(this.getActivity()).getSharedPreferences(SHARED_PREFS_POOL, MODE_PRIVATE);
        return sharedPreferences.getString(key, "0");
    }
}
