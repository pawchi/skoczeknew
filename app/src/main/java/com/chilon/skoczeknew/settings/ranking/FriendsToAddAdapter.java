package com.chilon.skoczeknew.settings.ranking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.chilon.skoczeknew.R;
import java.util.ArrayList;

public class FriendsToAddAdapter extends RecyclerView.Adapter<FriendsToAddAdapter.FriendsViewHolder> {
    private ArrayList<FriendItem> friendList;
    private LayoutInflater mInflater;
    private OnItemClickListener mListner;

    //clickable Item
    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListner = listener;
    }

    public FriendsToAddAdapter(Context context, ArrayList<FriendItem> friendList) {
        this.mInflater = LayoutInflater.from(context);
        this.friendList = friendList;
    }

    @NonNull
    @Override
    public FriendsToAddAdapter.FriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_friend_to_add, parent, false);
        return new FriendsToAddAdapter.FriendsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendsToAddAdapter.FriendsViewHolder holder, int position) {
        FriendItem currentItem = friendList.get(position);
        holder.userName.setText(currentItem.getUserName());
        holder.score.setText(String.valueOf(currentItem.getScore()));
        holder.city.setText(currentItem.getCity());
        holder.country.setText(currentItem.getCountry());
    }

    @Override
    public int getItemCount() {
        return friendList.size();
    }

    public class FriendsViewHolder extends RecyclerView.ViewHolder {
        TextView userName;
        TextView score;
        TextView country;
        TextView city;

        FriendsViewHolder(View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.id_friend_name);
            score = itemView.findViewById(R.id.id_friend_score);
            country = itemView.findViewById(R.id.id_friend_country);
            city = itemView.findViewById(R.id.id_friend_city);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListner.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}

