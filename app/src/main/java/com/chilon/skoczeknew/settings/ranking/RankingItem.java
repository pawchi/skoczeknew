package com.chilon.skoczeknew.settings.ranking;

import android.widget.ImageView;

public class RankingItem {
    private int position;
    private int userImage;
    private String userName;
    private int score;
    private String country;
    private String city;

    public RankingItem(int position, int userImage, String userName, int score, String country, String city) {
        this.position = position;
        this.userImage = userImage;
        this.userName = userName;
        this.score = score;
        this.country = country;
        this.city = city;
    }

    public RankingItem(String userName, String country, String city, int score) {
        this.userName = userName;
        this.country = country;
        this.city = city;
        this.score = score;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getUserImage() {
        return userImage;
    }

    public void setUserImage(int userImage) {
        this.userImage = userImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
