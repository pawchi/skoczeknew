package com.chilon.skoczeknew.settings.ranking;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chilon.skoczeknew.MyGlobals;
import com.chilon.skoczeknew.R;
import com.chilon.skoczeknew.settings.TinyDB;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AddFriendsFragment extends Fragment {

    EditText searchFriends;
    public static ArrayList<FriendItem> potentialFriendsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FriendsToAddAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_friend, container, false);

        searchFriends = rootView.findViewById(R.id.search_friends);
        searchFriends.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        hideSoftKeyboard(getActivity());
                        String enteredText = searchFriends.getText().toString();
                        if (checkIfStringMinLengthOK(enteredText)){
                            findFriendsInDB(enteredText);
                        } else {
                            createAlert(getResources().getString(R.string.search_friend_alert));
                        }

                        return true;
                    }
                }
                return false;
            }
        });
        recyclerView = rootView.findViewById(R.id.friends_recycler_view);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        searchFriends.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    searchFriends.setHint("");
                }
            }
        });
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void findFriendsInDB(String name) {
        DatabaseReference skoczekRef = FirebaseDatabase.getInstance().getReference("users");
        Query queryRef = skoczekRef.orderByKey().startAt(name).limitToFirst(10);
        potentialFriendsList.clear();
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot object : snapshot.getChildren()) {
                    String country = "null";
                    String city = "null";
                    String ranking = "null";
                    if (object.child("country").exists()){
                        country=object.child("country").getValue().toString();
                    }
                    if (object.child("city").exists()){
                        city=object.child("city").getValue().toString();
                    }
                    if (object.child("ranking").exists()){
                        ranking=object.child("ranking").getValue().toString();
                    }
                    potentialFriendsList.add(new FriendItem(object.getKey(), country,
                            city, Integer.parseInt(ranking)));
                }
                adapter = new FriendsToAddAdapter(getActivity(), potentialFriendsList);
                recyclerView.setAdapter(adapter);
                adapter.setOnItemClickListener(new FriendsToAddAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                            if (getListFromTinyDB(MyGlobals.FRIENDS_LIST_KEY).stream().noneMatch(o ->o.getUserName().equals(potentialFriendsList.get(position).getUserName()))){
                                ArrayList<FriendItem> tempFriendsList = getListFromTinyDB(MyGlobals.FRIENDS_LIST_KEY);
                                tempFriendsList.add(potentialFriendsList.get(position));
                                MyGlobals.FRIENDS_SECTION_ACTIVE = true;
                                saveFriendsListToTinyDB(tempFriendsList);
                                getActivity().getSupportFragmentManager().popBackStack();
                            } else {
                                Toast.makeText(getContext(), "User: "+potentialFriendsList.get(position).getUserName() + " is already your friend." , Toast.LENGTH_LONG).show();
                            }
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void saveFriendsListToTinyDB(ArrayList<FriendItem> friendsList){
        TinyDB tinyDB = new TinyDB(getContext());
        tinyDB.putListObject(MyGlobals.FRIENDS_LIST_KEY, friendsList);
    }

    private boolean checkIfStringMinLengthOK(String inputText){
        return inputText.trim().length() >= 3;
    }

    public void createAlert(String message) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setIcon(R.drawable.skoczek_transparent);
        final AlertDialog alert = dialog.create();
        alert.show();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    private ArrayList<FriendItem> getListFromTinyDB(String listKey){
        TinyDB tinyDB = new TinyDB(getContext());
        ArrayList<FriendItem> friendsList = new ArrayList<>();
        for (Object obj:tinyDB.getListObject(listKey,FriendItem.class)){
            FriendItem friendItem = (FriendItem) obj;
            friendsList.add(new FriendItem(friendItem.getUserName(),friendItem.getCountry(),friendItem.getCity(),friendItem.getScore()));
        }
        return friendsList;
    }
}
