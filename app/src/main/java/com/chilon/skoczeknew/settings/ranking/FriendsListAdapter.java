package com.chilon.skoczeknew.settings.ranking;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chilon.skoczeknew.MyGlobals;
import com.chilon.skoczeknew.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.FriendsListViewHolder> {
    private ArrayList<FriendItem> rankingList;
    private LayoutInflater mInflater;
    private OnItemClickListener mListner;

    //clickable Item
    public interface OnItemClickListener{
        void onDeleteClick(int position);

    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListner = listener;
    }

    public FriendsListAdapter(Context context, ArrayList<FriendItem> rankingList) {
        this.mInflater = LayoutInflater.from(context);
        this.rankingList = rankingList;
    }

    @NonNull
    @Override
    public FriendsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_friend_added, parent, false);
        return new FriendsListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendsListViewHolder holder, int position) {
        FriendItem currentItem = rankingList.get(position);
        holder.place.setText(String.valueOf(currentItem.getPosition()));
        holder.imageDelete.setImageResource(currentItem.getUserImage());
        holder.userName.setText(currentItem.getUserName());
        holder.score.setText(String.valueOf(currentItem.getScore()));
        holder.city.setText(currentItem.getCity());
        holder.country.setText(currentItem.getCountry());

        //highlight my user in ranking list
        if (currentItem.getUserName().toLowerCase().equals(loadSharedPrefs(MyGlobals.USER_NAME_KEY, holder).toLowerCase())) {
            holder.itemView.setBackgroundColor(Color.parseColor("#9ddef6"));
            holder.imageDelete.setVisibility(View.INVISIBLE);
            holder.imageDelete.setClickable(false);
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#e6e6fa"));
        }
        //add medal image for three first places
        if (currentItem.getPosition() == 1) {
            holder.place.setBackgroundResource(R.drawable.med_gold);
            holder.place.setText("");
        } else if (currentItem.getPosition() == 2) {
            holder.place.setBackgroundResource(R.drawable.med_silver);
            holder.place.setText("");
        } else if (currentItem.getPosition() == 3) {
            holder.place.setBackgroundResource(R.drawable.med_bronze);
            holder.place.setText("");
        } else {
            holder.place.setBackgroundResource(0);
        }
    }

    private String loadSharedPrefs(String key, FriendsListViewHolder holder) {
        SharedPreferences sharedPreferences = holder.imageDelete.getContext().getSharedPreferences(MyGlobals.SHARED_PREFS_POOL, MODE_PRIVATE);
        return sharedPreferences.getString(key, "0");
    }

    @Override
    public int getItemCount() {
        return rankingList.size();
    }

    public class FriendsListViewHolder extends RecyclerView.ViewHolder {
        TextView place;
        ImageView imageDelete;
        TextView userName;
        TextView score;
        TextView city;
        TextView country;

        FriendsListViewHolder(View itemView) {
            super(itemView);
            place = itemView.findViewById(R.id.id_friend_added_position);
            imageDelete = itemView.findViewById(R.id.id_friend_added_delete);
            userName = itemView.findViewById(R.id.id_friend_added_name);
            score = itemView.findViewById(R.id.id_friend_added_score);
            city = itemView.findViewById(R.id.id_friend_added_city);
            country = itemView.findViewById(R.id.id_friend_added_country);

            imageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListner.onDeleteClick(getAdapterPosition());
                }

            });
        }
    }
}
