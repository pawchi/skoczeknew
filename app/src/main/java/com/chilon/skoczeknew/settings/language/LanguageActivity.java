package com.chilon.skoczeknew.settings.language;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.chilon.skoczeknew.MainActivity;
import com.chilon.skoczeknew.R;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class LanguageActivity extends AppCompatActivity {

    private static final String SHARED_PREFS_POOL = "anySizePrefs";
    RadioGroup languageRadioGroup;
    RadioButton radioButton;
    String radioButtonResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_select_language);

        languageRadioGroup = (RadioGroup) findViewById(R.id.language_radioGroup);
        Button languageOkButton = findViewById(R.id.button_language_ok);
        Button languageCancelButton = findViewById(R.id.button_language_cancel);
        getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width * .7), (int) (height * .9));

        languageCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        languageOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButtonResult != null) {
                    saveLocale(radioButtonResult);
                    finish();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                } else {
                    finish();
                }
            }
        });

    }

    public void saveLocale(String languageCode) {
        SharedPreferences sharedPreferences = this.getSharedPreferences(SHARED_PREFS_POOL, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("My_Lang", languageCode);
        editor.apply();
    }
    public void loadLocale() {
        SharedPreferences prefs = getSharedPreferences(SHARED_PREFS_POOL, Activity.MODE_PRIVATE);
        String language = prefs.getString("My_Lang", "");
        setLanguageForApp(language);
    }

    public void setLanguageForApp(String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
    }

    public void rbSelLanguageOnClick(View view) {
        int languageCheckRadioGroup = languageRadioGroup.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(languageCheckRadioGroup);

        switch (radioButton.getId()) {
            case R.id.language_en:
                radioButtonResult = "en";
                break;
            case R.id.language_de:
                radioButtonResult = "de";
                break;
            case R.id.language_pl:
                radioButtonResult = "pl";
                //radioButtonResult = "pl_rPL";
                break;
            case R.id.language_es:
                radioButtonResult = "es";
                break;
            case R.id.language_fr:
                radioButtonResult = "fr";
                break;
            case R.id.language_pt:
                radioButtonResult = "pt";
                break;
            case R.id.language_ru:
                radioButtonResult = "ru";
                break;
            case R.id.language_zh:
                radioButtonResult = "zh";
                break;
            case R.id.language_vi:
                radioButtonResult = "vi";
                break;
            case R.id.language_cz:
                radioButtonResult = "cs";
                break;
            case R.id.language_it:
                radioButtonResult = "it";
                break;
            case R.id.language_tur:
                radioButtonResult = "tr";
                break;
            case R.id.language_taj:
                radioButtonResult = "th";
                break;
            case R.id.language_hg:
                radioButtonResult = "hu";
                break;
            case R.id.language_rum:
                radioButtonResult = "ro";
                break;
            case R.id.language_jap:
                radioButtonResult = "ja";
                break;
            case R.id.language_grec:
                radioButtonResult = "el";
                break;
            case R.id.language_hindi:
                radioButtonResult = "hi";
                break;
            case R.id.language_bengal:
                radioButtonResult = "bn";
                break;
            case R.id.language_slovak:
                radioButtonResult = "sk";
                break;
        }
    }
}
