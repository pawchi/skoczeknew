package com.chilon.skoczeknew.settings.ranking;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chilon.skoczeknew.BoardInfo;
import com.chilon.skoczeknew.ChooseBoardFragment;
import com.chilon.skoczeknew.MainActivity;
import com.chilon.skoczeknew.MyGlobals;
import com.chilon.skoczeknew.R;
import com.chilon.skoczeknew.settings.TinyDB;
import com.chilon.skoczeknew.settings.login.LoginOrRegisterFragment;
import com.chilon.skoczeknew.settings.login.UserDb;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static com.chilon.skoczeknew.ChooseBoardFragment.SHARED_PREFS_POOL;
import static com.chilon.skoczeknew.ChooseBoardFragment.boardInfosList;

public class RankingFragment extends Fragment {

    public static ArrayList<RankingItem> rankingItemList = new ArrayList<>();
    public static ArrayList<FriendItem> friendsRankingList = new ArrayList<>();
    private RecyclerView recyclerViewRanking;
    private RecyclerView recyclerViewFriends;
    private RecyclerView.LayoutManager layoutManagerRanking;
    private RecyclerView.LayoutManager layoutManagerFriends;
    private RankingAdapter adapterRanking;
    private FriendsListAdapter adapterFriends;
    ProgressDialog progress;
    HashMap<String, String> gameStatus = new HashMap<>();
    TextView cityTextView, countryTextView, worldTextView, friendsTextView;
    LinearLayout mainLayout;
    FirebaseFunctions mFunctions;
    String resultFBFunctions = "";
    FloatingActionButton addFriendsButton;
    TextView labelPosition, labelUser, labelScore, labelCity, labelCountry, labelRemoveFriend;
    int loopCountingUpdateFriends;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ranking, container, false);

        mFunctions = FirebaseFunctions.getInstance();
        mainLayout = rootView.findViewById(R.id.ranking_main_layout);
        cityTextView = rootView.findViewById(R.id.ranking_city);
        countryTextView = rootView.findViewById(R.id.ranking_country);
        worldTextView = rootView.findViewById(R.id.ranking_world);
        friendsTextView = rootView.findViewById(R.id.ranking_friends);
        addFriendsButton = rootView.findViewById(R.id.addFriend);

        worldTextView.setTextColor(getResources().getColor(R.color.active_ranking_list));
        countryTextView.setTextColor(getResources().getColor(R.color.passive_ranking_list));
        cityTextView.setTextColor(getResources().getColor(R.color.passive_ranking_list));
        friendsTextView.setTextColor(getResources().getColor(R.color.passive_ranking_list));

        recyclerViewRanking = rootView.findViewById(R.id.ranking_recycler_view);
        layoutManagerRanking = new LinearLayoutManager(getActivity());
        recyclerViewRanking.setLayoutManager(layoutManagerRanking);

        recyclerViewFriends = rootView.findViewById(R.id.friends_recycler_view);
        layoutManagerFriends = new LinearLayoutManager(getActivity());
        recyclerViewFriends.setLayoutManager(layoutManagerFriends);

        initializeLabels(rootView);

        addFriendsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new AddFriendsFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        cityTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setActiveRankingSection();
                cityTextView.setTextColor(getResources().getColor(R.color.active_ranking_list));
                setLabelsParamsByOthers();
                loadCityOrCountryList("city", MyGlobals.CITY_KEY);
            }
        });
        countryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setActiveRankingSection();
                countryTextView.setTextColor(getResources().getColor(R.color.active_ranking_list));
                setLabelsParamsByOthers();
                loadCityOrCountryList("country", MyGlobals.COUNTRY_KEY);
            }
        });
        worldTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setActiveRankingSection();
                worldTextView.setTextColor(getResources().getColor(R.color.active_ranking_list));
                setLabelsParamsByOthers();
                loadWorldList();
            }
        });
        friendsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateSectionFriends();
            }
        });
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        currentRankingPoints();
        if (FirebaseAuth.getInstance().getUid() != null) {
            if (MyGlobals.FRIENDS_SECTION_ACTIVE) {
                activateSectionFriends();
            } else {
                try {
                    addUserDataToDataBase();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                TextView worldTextView = view.findViewById(R.id.ranking_world);
                worldTextView.setTextColor(getResources().getColor(R.color.active_ranking_list));
            }

        } else {
            createAlertIfUserNotRegisterd();
        }
    }

    private void activateSectionFriends() {
        setActiveRankingSection();
        friendsTextView.setTextColor(getResources().getColor(R.color.active_ranking_list));
        addFriendsButton.setVisibility(View.VISIBLE);
        setLabelsParamsByFriends();
        loadFriendsList();
    }

    private int convertPixelsToDp(int dps){
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dps * scale + 0.5f);
    }

    private void setLabelsParamsByFriends(){
        ViewGroup.LayoutParams posParams = labelPosition.getLayoutParams();
        posParams.width = convertPixelsToDp(35);
        labelPosition.setLayoutParams(posParams);

        ViewGroup.LayoutParams posUser = labelUser.getLayoutParams();
        posUser.width = convertPixelsToDp(70);
        labelUser.setLayoutParams(posUser);

        ViewGroup.LayoutParams posScore = labelScore.getLayoutParams();
        posScore.width = convertPixelsToDp(55);
        labelScore.setLayoutParams(posScore);

        labelRemoveFriend.setVisibility(View.VISIBLE);

    }

    private void setLabelsParamsByOthers(){
        ViewGroup.LayoutParams posParams = labelPosition.getLayoutParams();
        posParams.width = convertPixelsToDp(55);
        labelPosition.setLayoutParams(posParams);

        ViewGroup.LayoutParams posUser = labelUser.getLayoutParams();
        posUser.width = convertPixelsToDp(75);
        labelUser.setLayoutParams(posUser);

        ViewGroup.LayoutParams posScore = labelScore.getLayoutParams();
        posScore.width = convertPixelsToDp(50);
        labelScore.setLayoutParams(posScore);

        labelRemoveFriend.setVisibility(View.GONE);
    }

    private void initializeLabels(View view){
        labelCity = view.findViewById(R.id.id_label_city);
        labelCountry = view.findViewById(R.id.id_label_country);
        labelPosition = view.findViewById(R.id.id_label_position);
        labelScore = view.findViewById(R.id.id_label_ranking_score);
        labelUser = view.findViewById(R.id.id_label_user_name);
        labelRemoveFriend = view.findViewById(R.id.id_label_remove_friend);
    }

    private void setActiveRankingSection() {
        cityTextView.setTextColor(getResources().getColor(R.color.passive_ranking_list));
        countryTextView.setTextColor(getResources().getColor(R.color.passive_ranking_list));
        worldTextView.setTextColor(getResources().getColor(R.color.passive_ranking_list));
        friendsTextView.setTextColor(getResources().getColor(R.color.passive_ranking_list));

        addFriendsButton.setVisibility(View.INVISIBLE);
    }

    private void loadFriendsList() {
        friendsRankingList.clear();
        showProgress();
        if (getListFromTinyDB(MyGlobals.FRIENDS_LIST_KEY).size() > 0) {
            for (FriendItem friendItem : getListFromTinyDB(MyGlobals.FRIENDS_LIST_KEY)) {
                friendsRankingList.add(new FriendItem(0, friendItem.getUserName(), friendItem.getScore(), friendItem.getCity(), friendItem.getCountry(), R.drawable.ic_delete_friend));
            }

        } else {
            Log.d("tinyDB", " has no records.");
        }
        if (friendsRankingList.stream().noneMatch(i -> i.getUserName().equals(loadSharedPrefs(MyGlobals.USER_NAME_KEY)))){
            friendsRankingList.add(new FriendItem(loadSharedPrefs(MyGlobals.USER_NAME_KEY), Integer.parseInt(loadSharedPrefs(MyGlobals.TOTAL_RANKING_KEY)),
                    loadSharedPrefs(MyGlobals.CITY_KEY),loadSharedPrefs(MyGlobals.COUNTRY_KEY),R.drawable.ic_delete_friend));
        }
        loopCountingUpdateFriends=0;
        for (int x=0;x<friendsRankingList.size();x++){
            loopCountingUpdateFriends++;
            String friend = friendsRankingList.get(x).getUserName();
            int friendRanking = friendsRankingList.get(x).getScore();
            actualizeFriendsRankingWithDB(friend,friendRanking);
        }
    }

    private void actualizeFriendsRankingWithDB(String friend, int friendRanking){
        DatabaseReference friendRef = FirebaseDatabase.getInstance().getReference("users").child(friend);
        friendRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    if (!snapshot.child("ranking").getValue().toString().equals(Integer.toString(friendRanking))){
                        for (int i=0;i<friendsRankingList.size();i++){
                            if (friendsRankingList.get(i).getUserName().equals(friend)){
                                friendsRankingList.get(i).setScore(Integer.parseInt(snapshot.child("ranking").getValue().toString()));
                            }
                        }
                    }
                }

                if (loopCountingUpdateFriends==friendsRankingList.size()){
                    saveFriendsListToTinyDB(friendsRankingList);
                    sortPositionsOfFriendsList();
                    adapterFriends = new FriendsListAdapter(getActivity(), friendsRankingList);
                    recyclerViewFriends.setAdapter(adapterFriends);
                    recyclerViewRanking.setVisibility(View.GONE);
                    recyclerViewFriends.setVisibility(View.VISIBLE);
                    adapterFriends.setOnItemClickListener(new FriendsListAdapter.OnItemClickListener() {
                        @Override
                        public void onDeleteClick(int position) {
                            friendsRankingList.remove(position);
                            sortPositionsOfFriendsList();
                            adapterFriends.notifyDataSetChanged();
                            saveFriendsListToTinyDB(friendsRankingList);
                        }
                    });
                    progress.dismiss();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void sortPositionsOfFriendsList(){
        Collections.sort(friendsRankingList, (o1, o2) -> o2.getScore() - o1.getScore());
        int pos = 1;
        for (FriendItem item : friendsRankingList) {
            item.setPosition(pos);
            pos++;
        }
    }

    private void loadWorldList() {
        Map<String, Object> data = new HashMap<>();
        data.put("name", loadSharedPrefs(MyGlobals.USER_NAME_KEY));
        data.put("city", loadSharedPrefs(MyGlobals.CITY_KEY));
        data.put("country", loadSharedPrefs(MyGlobals.COUNTRY_KEY));

        showProgress();
        DatabaseReference skoczekRef = FirebaseDatabase.getInstance().getReference("users");
        rankingItemList.clear();
        Query queryRef = skoczekRef.orderByChild("ranking").limitToLast(50);
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                long position = snapshot.getChildrenCount();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    String ranking = dataSnapshot.child("ranking").getValue().toString();
                    String userName = dataSnapshot.getKey();
                    String city = "none";
                    if (dataSnapshot.hasChild("city")) {
                        city = dataSnapshot.child("city").getValue().toString();
                        if (city.equals("0")) {
                            city = "none";
                        }
                    }
                    String country = "none";
                    if (dataSnapshot.hasChild("country")) {
                        country = dataSnapshot.child("country").getValue().toString();
                        if (country.equals("0")) {
                            country = "none";
                        }
                    }
                    rankingItemList.add(new RankingItem((int) position, R.drawable.ic_language, userName, Integer.parseInt(ranking), city, country));
                    if (userName.equals(loadSharedPrefs(MyGlobals.USER_NAME_KEY))) {
                        saveRankingInSharedPrefs(MyGlobals.GLOBAL_POSITION_KEY, String.valueOf(position));
                    }
                    position--;
                }
                Collections.reverse(rankingItemList);

                mFunctions = FirebaseFunctions.getInstance();
                mFunctions
                        .getHttpsCallable("getUserOnCall")
                        .call(data)
                        .addOnSuccessListener(getActivity(), new OnSuccessListener<HttpsCallableResult>() {
                            @Override
                            public void onSuccess(HttpsCallableResult httpsCallableResult) {

                                resultFBFunctions = httpsCallableResult.getData().toString();

                                try {
                                    JSONObject jsonObject = new JSONObject(resultFBFunctions);
                                    saveRankingInSharedPrefs(MyGlobals.GLOBAL_POSITION_KEY, jsonObject.getString("rankWorld"));
                                    saveRankingInSharedPrefs(MyGlobals.COUNTRY_POSITION_KEY, jsonObject.getString("rankCountry"));
                                    saveRankingInSharedPrefs(MyGlobals.CITY_POSITION_KEY, jsonObject.getString("rankCity"));
                                    ((MainActivity) getActivity()).appBarRanking();
                                    if (!containsName(rankingItemList, loadSharedPrefs(MyGlobals.USER_NAME_KEY))) {
                                        rankingItemList.add(new RankingItem(Integer.parseInt(loadSharedPrefs(MyGlobals.GLOBAL_POSITION_KEY)), R.drawable.ic_language, data.get("name").toString(),
                                                Integer.parseInt(loadSharedPrefs(MyGlobals.TOTAL_RANKING_KEY)), data.get("city").toString(), data.get("country").toString()));
                                    }
                                    recyclerViewRanking.setVisibility(View.VISIBLE);
                                    recyclerViewFriends.setVisibility(View.GONE);
                                    adapterRanking = new RankingAdapter(getActivity(), rankingItemList);
                                    recyclerViewRanking.setAdapter(adapterRanking);
                                    progress.dismiss();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }

    public boolean containsName(ArrayList<RankingItem> list, String name) {
        return list.stream().anyMatch(o -> o.getUserName().equals(name));
    }

    private void loadCityOrCountryList(String orderBy, String shPrefkey) {
        rankingItemList.clear();

        Map<String, Object> data = new HashMap<>();
        data.put("returnKey", orderBy);
        data.put("name", loadSharedPrefs(MyGlobals.USER_NAME_KEY));
        data.put("city", loadSharedPrefs(MyGlobals.CITY_KEY));
        data.put("country", loadSharedPrefs(MyGlobals.COUNTRY_KEY));
        showProgress();
        mFunctions = FirebaseFunctions.getInstance();
        mFunctions.getHttpsCallable("getUsersFromCountry").call(data)
                .addOnSuccessListener(getActivity(), new OnSuccessListener<HttpsCallableResult>() {
                    @Override
                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                        resultFBFunctions = httpsCallableResult.getData().toString();
                        try {
                            JSONObject json = new JSONObject(resultFBFunctions);
                            JSONArray jsonArray = json.getJSONArray("response");
                            for (int n = 0; n < jsonArray.length(); n++) {
                                JSONObject object = jsonArray.getJSONObject(n);
                                rankingItemList.add(new RankingItem(n + 1,
                                        R.drawable.ic_language,
                                        object.getString("name"), +
                                        Integer.parseInt(object.getString("ranking")),
                                        object.getString("city"),
                                        object.getString("country")));
                            }
                            if (!containsName(rankingItemList, loadSharedPrefs(MyGlobals.USER_NAME_KEY))) {
                                rankingItemList.add(new RankingItem(Integer.parseInt(loadSharedPrefs(shPrefkey + "_position")),
                                        R.drawable.ic_language, loadSharedPrefs(MyGlobals.USER_NAME_KEY), Integer.parseInt(loadSharedPrefs(MyGlobals.TOTAL_RANKING_KEY)),
                                        loadSharedPrefs(MyGlobals.CITY_KEY), loadSharedPrefs(MyGlobals.COUNTRY_KEY)));
                            }
                            recyclerViewRanking.setVisibility(View.VISIBLE);
                            recyclerViewFriends.setVisibility(View.GONE);
                            adapterRanking.notifyDataSetChanged();
                            recyclerViewRanking.setAdapter(adapterRanking);
                            progress.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progress.dismiss();
                    }
                });
    }

    public void addUserDataToDataBase() throws UnsupportedEncodingException {
        showProgress();
        prepareGameStatus();
        int ranking = Integer.parseInt(loadSharedPrefs(MyGlobals.TOTAL_RANKING_KEY));
        String userName = loadSharedPrefs(MyGlobals.USER_NAME_KEY);
        //String userName = "";
//        if (MyGlobals.stringContainsSpecialCharacter(loadSharedPrefs(MyGlobals.USER_NAME_KEY))){
//            userName = MyGlobals.decodeSpecialCharacters(loadSharedPrefs(MyGlobals.USER_NAME_KEY));
//            Log.d("decoded***********", userName + "     *******************");
//        } else {
//            userName = loadSharedPrefs(MyGlobals.USER_NAME_KEY);
//        }
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference myRef = db.getReference("users").child(userName);
        String city = loadSharedPrefs(MyGlobals.CITY_KEY);
        String country = loadSharedPrefs(MyGlobals.COUNTRY_KEY);
        UserDb userDb = new UserDb(ranking, city, country, gameStatus);
        myRef.setValue(userDb)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        progress.dismiss();
                        loadWorldList();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Something goes wrong :( ", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void prepareGameStatus() {
        gameStatus.put("5 x 5", loadSharedPrefs("5 x 5"));
        gameStatus.put("5 x 6", loadSharedPrefs("5 x 6"));
        gameStatus.put("5 x 7", loadSharedPrefs("5 x 7"));
        gameStatus.put("5 x 8", loadSharedPrefs("5 x 8"));
        gameStatus.put("5 x 9", loadSharedPrefs("5 x 9"));
        gameStatus.put("6 x 6", loadSharedPrefs("6 x 6"));
        gameStatus.put("6 x 7", loadSharedPrefs("6 x 7"));
        gameStatus.put("6 x 8", loadSharedPrefs("6 x 8"));
        gameStatus.put("6 x 9", loadSharedPrefs("6 x 9"));
        gameStatus.put("7 x 7", loadSharedPrefs("7 x 7"));
        gameStatus.put("7 x 8", loadSharedPrefs("7 x 8"));
        gameStatus.put("7 x 9", loadSharedPrefs("7 x 9"));
        gameStatus.put("8 x 8", loadSharedPrefs("8 x 8"));
        gameStatus.put("8 x 9", loadSharedPrefs("8 x 9"));
        gameStatus.put("9 x 9", loadSharedPrefs("9 x 9"));
    }

    public void createAlertIfUserNotRegisterd() {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.ranking_no_user))
                .setMessage(getString(R.string.ranking_alert_no_user))
                .setIcon(R.drawable.skoczek_transparent)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        navigateToLoginOrRegisterFragment();
                    }
                })
                .setNegativeButton(getString(R.string.not), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        navigateToChooseBoardFragment();
                    }
                }).show();
    }

    public void navigateToLoginOrRegisterFragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, new LoginOrRegisterFragment()).commit();
    }

    public void navigateToChooseBoardFragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, new ChooseBoardFragment()).commit();
    }

    private void showProgress() {
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Syncing");
        progress.setCancelable(false);
        progress.show();
    }

    private String currentRankingPoints() {
        int totalRanking = 0;
        for (BoardInfo boardInfo : boardInfosList) {
            int singleBoardResult = Integer.parseInt(loadSharedPrefs(boardInfo.getSize()));
            totalRanking += singleBoardResult;
        }
        saveRankingInSharedPrefs(MyGlobals.TOTAL_RANKING_KEY, Integer.toString(totalRanking));
        return Integer.toString(totalRanking);
    }

    private void saveFriendsListToTinyDB(ArrayList<FriendItem> friendsList){
        TinyDB tinyDB = new TinyDB(getContext());
        tinyDB.putListObject(MyGlobals.FRIENDS_LIST_KEY, friendsList);
    }

    private ArrayList<FriendItem> getListFromTinyDB(String listKey) {
        TinyDB tinyDB = new TinyDB(getContext());
        ArrayList<FriendItem> friendsList = new ArrayList<>();
        for (Object obj : tinyDB.getListObject(listKey, FriendItem.class)) {
            FriendItem friendItem = (FriendItem) obj;
            friendsList.add(new FriendItem(friendItem.getUserName(), friendItem.getCountry(), friendItem.getCity(), friendItem.getScore()));
        }
        return friendsList;
    }

    public String loadSharedPrefs(String key) {
        SharedPreferences sharedPreferences = Objects.requireNonNull(this.getActivity()).getSharedPreferences(SHARED_PREFS_POOL, MODE_PRIVATE);
        return sharedPreferences.getString(key, "0");
    }

    public void saveRankingInSharedPrefs(String key, String ranking) {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(SHARED_PREFS_POOL, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, ranking);
        editor.apply();
    }
}
