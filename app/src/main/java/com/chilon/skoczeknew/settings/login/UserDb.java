package com.chilon.skoczeknew.settings.login;

import java.util.HashMap;

public class UserDb {
    private String userName;
    private int ranking;
    private String city;
    private String country;
    private HashMap<String, String> gameStatus = new HashMap<>();

    public UserDb(String userName, int ranking) {
        this.userName = userName;
        this.ranking = ranking;
    }

    public UserDb(int ranking){
        this.ranking=ranking;
    }

    public UserDb(int ranking, HashMap<String, String> gameStatus){
        this.ranking=ranking;
        this.gameStatus = gameStatus;
    }

    public UserDb(String userName, int ranking, HashMap<String, String> gameStatus) {
        this.userName = userName;
        this.ranking = ranking;
        this.gameStatus = gameStatus;
    }

    public UserDb( int ranking, String city, String country, HashMap<String, String> gameStatus) {
        this.ranking = ranking;
        this.city = city;
        this.country = country;
        this.gameStatus = gameStatus;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public HashMap<String, String> getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(HashMap<String, String> gameStatus) {
        this.gameStatus = gameStatus;
    }
}
