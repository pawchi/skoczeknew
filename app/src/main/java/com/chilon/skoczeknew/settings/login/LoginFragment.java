package com.chilon.skoczeknew.settings.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.chilon.skoczeknew.MainActivity;
import com.chilon.skoczeknew.MyGlobals;
import com.chilon.skoczeknew.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;

public class LoginFragment extends Fragment {
    private static final String SHARED_PREFS_POOL = "anySizePrefs";
    private EditText emailInput, passwordInput;
    private FirebaseAuth mAuth;
    ProgressDialog progress;
    private Button buttonForgotPass;

    GoogleSignInClient mGoogleSignInClient;
    String userName;
    private HashMap<String, String> gameStatusDB = new HashMap<>();
    Intent intent;
    private static final int RC_SIGN_IN = 100;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        intent= new Intent(this.getActivity(), MainActivity.class);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailInput = view.findViewById(R.id.inputEmailLoginFragment);
        passwordInput = view.findViewById(R.id.inputPasswordLoginFragment);
        buttonForgotPass = view.findViewById(R.id.forgot_pass_button);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this.getActivity(), gso);


        view.findViewById(R.id.google_sign_in_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGoogle();
            }
        });

        view.findViewById(R.id.loginBtnLoginFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyGlobals.isEmailValid(emailInput.getText().toString())){
                    loginUser();
                } else {
                    Toast.makeText(getContext(), "" + getResources().getString(R.string.invalid_email), Toast.LENGTH_SHORT).show();
                }

            }
        });

        buttonForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new SendLinkToChangePass()).commit();
            }
        });
    }

    private void signInWithGoogle(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d("ID: ", "firebaseAuthWithGoogle SUCCESSFUL. Account ID :" + account.getId());
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.v("TAG", "Google sign in failed"+ e);
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            showProgress();
                            getUserInfoFromFireBase();
                        } else {
                            Toast.makeText(requireContext(), "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void loginUser() {
        String email = emailInput.getText().toString();
        String pass = passwordInput.getText().toString();
        if (!email.isEmpty() && !pass.isEmpty()) {
            loginFBUser(email, pass);
        } else {
            Toast.makeText(requireContext(), "Fill data - both fields can not be empty.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void loginFBUser(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            getUserInfoFromFireBase();
                        } else {
                            Toast.makeText(requireContext(), "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void getUserInfoFromFireBase(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user!=null){
            userName = user.getDisplayName().toLowerCase();
        }
        saveSharedPrefs(MyGlobals.USER_NAME_KEY, userName);
        DatabaseReference skoczekRef = FirebaseDatabase.getInstance().getReference("users").child(userName);

        skoczekRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot: snapshot.child("gameStatus").getChildren()){
                    String boardScore = dataSnapshot.getValue().toString();
                    String boardKey = dataSnapshot.getKey().toString();
                    intent.putExtra(boardKey, boardScore);
                }
                if (snapshot.hasChild("city")){
                    String city = snapshot.child("city").getValue().toString();
                    intent.putExtra("city", city);
                }
                if (snapshot.hasChild("country")){
                    String country = snapshot.child("country").getValue().toString();
                    intent.putExtra("country", country);
                }
                progress.dismiss();
                refreshActivity();
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }

    private void showProgress() {
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Syncing");
        progress.setCancelable(false);
        progress.show();
    }

    private void refreshActivity(){
        getActivity().finish();
        getActivity().overridePendingTransition(0, 0); //no blink after recreate
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0); //no blink after recreate
    }

    public void saveSharedPrefs(String key, String userName){
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(SHARED_PREFS_POOL, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, userName);
        editor.apply();
    }
}
