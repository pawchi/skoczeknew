package com.chilon.skoczeknew.settings.login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.chilon.skoczeknew.ChooseBoardFragment;
import com.chilon.skoczeknew.MainActivity;
import com.chilon.skoczeknew.MyGlobals;
import com.chilon.skoczeknew.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.UnsupportedEncodingException;

import static android.content.Context.MODE_PRIVATE;
import static com.chilon.skoczeknew.ChooseBoardFragment.SHARED_PREFS_POOL;

public class RegisterFragment extends Fragment {
    private EditText emailInput, passwordInput, userNameInput;
    private FirebaseAuth mAuth;
    private View loader;
    String userName;
    Intent intent;
    GoogleSignInClient mGoogleSignInClient;
    private boolean userNameExists = true;
    private static final int RC_SIGN_IN = 100;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        intent= new Intent(this.getActivity(), MainActivity.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailInput = view.findViewById(R.id.inputEmailRegister);
        passwordInput = view.findViewById(R.id.inputPasswordRegister);
        userNameInput = view.findViewById(R.id.inputUserNameRegister);

        loader = view.findViewById(R.id.loader_register);
        loader.setVisibility(View.INVISIBLE);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this.getActivity(), gso);

        view.findViewById(R.id.google_sign_in_fragment_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGoogle();
            }
        });

        view.findViewById(R.id.registerBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyGlobals.isEmailValid(emailInput.getText().toString())){
                        registerUser();
                } else {
                    Toast.makeText(getContext(), "" + getResources().getString(R.string.invalid_email), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void signInWithGoogle(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.v("TAG", "Google sign in failed"+ e);
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();

                            if (checkIfGoogleUserExistsInDataBase()){
                                getUserInfoFromFireBase();
                                loader.setVisibility(View.INVISIBLE);
                            } else {
                                String googleUserName = user.getDisplayName().toLowerCase();
                                String userName="";
                                if (MyGlobals.stringContainsSpecialCharacter(googleUserName)){
                                    userName = MyGlobals.decodeSpecialCharacters(googleUserName);
                                } else {
                                    userName = googleUserName;
                                }

                                setUserDisplayNameInFB(userName);
                                saveSharedPrefs(MyGlobals.USER_NAME_KEY, userName);
                                addUserNameToFireBaseDataBase(userName);
                                loader.setVisibility(View.INVISIBLE);
                                refreshActivity();
                            }
                        } else {
                            alertUserNotCreated(getResources().getString(R.string.alert_account_already_exists));
                        }
                    }
                });
    }

    public boolean checkIfGoogleUserExistsInDataBase(){
        DatabaseReference skoczekRef = FirebaseDatabase.getInstance().getReference("users");
        skoczekRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                userNameExists = snapshot.child(userName).exists();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        return userNameExists;
    }

    public void getUserInfoFromFireBase(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user!=null){
            userName = "";
            if (MyGlobals.stringContainsSpecialCharacter(user.getDisplayName().toLowerCase())){
                userName = MyGlobals.decodeSpecialCharacters(user.getDisplayName().toLowerCase());
            } else {
                userName = userNameInput.getText().toString().toLowerCase();
            }
        }
        saveSharedPrefs(MyGlobals.USER_NAME_KEY, userName);
        DatabaseReference skoczekRef = FirebaseDatabase.getInstance().getReference("users").child(userName);

        skoczekRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot: snapshot.child("gameStatus").getChildren()){
                    String boardScore = dataSnapshot.getValue().toString();
                    String boardKey = dataSnapshot.getKey().toString();
                    intent.putExtra(boardKey, boardScore);
                }
                if (snapshot.hasChild("city")){
                    String city = snapshot.child("city").getValue().toString();
                    intent.putExtra("city", city);
                }
                if (snapshot.hasChild("country")){
                    String country = snapshot.child("country").getValue().toString();
                    intent.putExtra("country", country);
                }

                refreshActivity();
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }

    public void registerUser()  {
        String email = emailInput.getText().toString();
        String pass = passwordInput.getText().toString();
        String userName = "";
        if (MyGlobals.stringContainsSpecialCharacter(userNameInput.getText().toString().toLowerCase())){
            userName = MyGlobals.decodeSpecialCharacters(userNameInput.getText().toString().toLowerCase());
        } else {
            userName = userNameInput.getText().toString().toLowerCase();
        }

        if (!email.isEmpty() && !pass.isEmpty() && !userName.isEmpty()) {
            checkIfUserNameUnique(email, pass, userName);
        } else {
                Toast.makeText(requireContext(), "All fields can't be empty!",
                        Toast.LENGTH_SHORT).show();
            }
    }

    public void checkIfUserNameUnique(final String email, final String pass, final String userName){
        DatabaseReference skoczekRef = FirebaseDatabase.getInstance().getReference("users");
        skoczekRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                userNameExists = snapshot.child(userName).exists();
                if (userNameExists){
                    alertUserNotCreated(getResources().getString(R.string.alert_user_already_exists));
                } else {
                    registerFireBaseUser(email, pass, userName);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void registerFireBaseUser(final String email, final String password, final String username){
        loader.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            setUserDisplayNameInFB(username);
                            saveSharedPrefs(MyGlobals.USER_NAME_KEY, username);
                            loader.setVisibility(View.INVISIBLE);
                            addUserNameToFireBaseDataBase(username);
                            loginFBUser(email, password);
                            Toast.makeText(getContext(), "User created OK", Toast.LENGTH_SHORT).show();
                        } else {
                            alertUserNotCreated(getResources().getString(R.string.alert_email_already_exists));
                        }
                    }
                });
    }

    public void setUserDisplayNameInFB(String username){
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder().setDisplayName(username.toLowerCase()).build();
        user.updateProfile(profileUpdate);
    }
    public void addUserNameToFireBaseDataBase(String userName){
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference myRef = db.getReference("users").child(userName.toLowerCase());
        UserDb userDb = new UserDb(0); // "0" is initial ranking
        myRef.setValue(userDb);
    }

    public void alertUserNotCreated(String message){
        new AlertDialog.Builder(getContext())
                .setTitle("User not unique")
                .setMessage(message)
                .setIcon(R.drawable.skoczek_transparent)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        loader.setVisibility(View.INVISIBLE);
                    }
                })
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loader.setVisibility(View.INVISIBLE);
                    }
                })
                .setNegativeButton(getString(R.string.not), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        navigateToChooseBoard();
                    }
                }).show();
    }

    private void loginFBUser(String email, String password) {
        loader.setVisibility(View.VISIBLE);
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        loader.setVisibility(View.INVISIBLE);
                        if (task.isSuccessful()) {
                            refreshActivity();
                        } else {
                            Toast.makeText(requireContext(), "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void refreshActivity(){
        getActivity().finish();
        getActivity().overridePendingTransition(0, 0); //no blink after recreate
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0); //no blink after recreate
    }

    public void saveSharedPrefs(String key, String userName){
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(SHARED_PREFS_POOL, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, userName);
        editor.apply();
    }
    private void navigateToChooseBoard(){
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new ChooseBoardFragment())
                .commit();
    }



}
