package com.chilon.skoczeknew.settings.manual;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.chilon.skoczeknew.ChooseBoardFragment;
import com.chilon.skoczeknew.MyGlobals;
import com.chilon.skoczeknew.R;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.WINDOW_SERVICE;

public class ManualFragment extends Fragment {

    ImageView handClick, handNoClick, playPause, manualBackgroundView;
    TextView textManual, textManualFinish;
    LinearLayout linearLayoutForAnimation, manualLayoutFinish;
    AnimatorSet animSetXY;
    boolean pause = false;
    int animNo=0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_manual, container, false);

        handClick = rootView.findViewById(R.id.id_manual_hand_click);
        handNoClick = rootView.findViewById(R.id.id_manual_hand_no_click);
        linearLayoutForAnimation = rootView.findViewById(R.id.manual_layout_for_anim);
        playPause = rootView.findViewById(R.id.id_manual_play_pause_image);
        manualBackgroundView = rootView.findViewById(R.id.id_manual_back_view);
        textManual = rootView.findViewById(R.id.text_manual);
        textManualFinish = rootView.findViewById(R.id.manual_finish_text);
        manualLayoutFinish = rootView.findViewById(R.id.manual_layout_finish);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        saveSharedPrefs(MyGlobals.SHOW_MANUAL_BY_EVERY_START,"yes");
        moveObjectAnimator(handNoClick,50, 50, 15, 10, 1, 2000);

        linearLayoutForAnimation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pause){
                    animSetXY.pause();
                    pause = true;
                    playPause.setImageResource(R.drawable.play_transp);
                } else {
                    pause = false;
                    animSetXY.resume();
                    playPause.setImageResource(R.drawable.pause_transp);
                }

            }
        });


    }

    private void moveObjectAnimator(ImageView image, int xFrom, int yFrom, int xTo, int yTo, int nextAnimation, int duration){
        int targetX = (scrWidthInPixels()*xTo)/100;
        int targetY = (scrHeightInPixels()*yTo)/100;
        int startX = (scrWidthInPixels()*xFrom)/100;
        int startY = (scrHeightInPixels()*yFrom)/100;

        animSetXY = new AnimatorSet();
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(image,"x",startX,targetX);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(image,"y",startY,targetY);
        animSetXY.playTogether(animatorX,animatorY);
        animSetXY.setDuration(duration);
        animSetXY.start();

        if (duration>1000 || duration<10){ //pause for every second animation
            animSetXY.pause();
            pause = true;
            playPause.setImageResource(R.drawable.play_transp);
        }
        if (duration==3000){ //last animation before finish
            pause = false;
            animSetXY.resume();
        }


            animSetXY.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    switch (nextAnimation){
                        case 1:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,15, 10, 15, 10, 2, 600);
                            break;
                        case 2:
                            setDelayedText(getText(R.string.manual_info_text2).toString(),500);
                            handClick.setVisibility(View.INVISIBLE);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_2);
                            moveObjectAnimator(handNoClick,50, 50, 8, 20, 3, 2000);
                            break;
                        case 3:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,8, 20, 8, 20, 4, 600);
                            break;
                        case 4:
                            setDelayedText(getText(R.string.manual_info_text3).toString(),500);
                            handClick.setVisibility(View.INVISIBLE);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_3);
                            moveObjectAnimator(handNoClick,50, 50, 44, 33, 5, 2000);
                            break;
                        case 5:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,44, 33, 44, 33, 6, 600);
                            break;

                        case 6:
                            setDelayedText(getText(R.string.manual_info_text4).toString(),500);
                            handClick.setVisibility(View.INVISIBLE);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_4);
                            moveObjectAnimator(handNoClick,50, 50, 80, 20, 7, 2000);
                            break;
                        case 7:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,80, 20, 80, 20, 8, 600);
                            break;
                        case 8:
                            setDelayedText(getText(R.string.manual_info_text5).toString(),10);
                            moveObjectAnimator(handClick,80, 20, 80, 20, 9, 1);
                            break;
                        case 9:
                            setDelayedText(getText(R.string.manual_info_text6).toString(),10);
                            moveObjectAnimator(handClick,80, 20, 80, 20, 10, 1);
                            break;
                        case 10:
                            setDelayedText(getText(R.string.manual_info_text7).toString(),500);
                            handClick.setVisibility(View.INVISIBLE);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_5);
                            moveObjectAnimator(handNoClick,50, 50, 6, 2, 11, 2000);
                            break;
                        case 11:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,6, 2, 6, 2, 12, 600);
                            break;
                        case 12:
                            handClick.setVisibility(View.INVISIBLE);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_6);
                            moveObjectAnimator(handNoClick,50, 50, 25, 23, 13, 2000);
                            break;
                        case 13:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,25, 23, 25, 23, 14, 600);
                            break;
                        case 14:
                            handClick.setVisibility(View.INVISIBLE);
                            setDelayedText(getText(R.string.manual_info_text8).toString(),300);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_7);
                            moveObjectAnimator(handNoClick,50, 50, 11, 9, 15, 2000);
                            break;
                        case 15:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,11, 9, 11, 9, 16, 600);
                            break;
                        case 16:
                            handClick.setVisibility(View.INVISIBLE);
                            setDelayedText(getText(R.string.manual_info_text9).toString(),300);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_8);
                            moveObjectAnimator(handNoClick,50, 50, 80, 76, 17, 2000);
                            break;
                        case 17:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,80, 76, 80, 76, 18, 600);
                            break;

                        case 18:
                            handClick.setVisibility(View.INVISIBLE);
                            setDelayedText(getText(R.string.manual_info_text10).toString(),300);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_9);
                            moveObjectAnimator(handNoClick,50, 50, 8, 10, 19, 2000);
                            break;
                        case 19:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,8, 10, 8, 10, 20, 600);
                            break;
                        case 20:
                            handClick.setVisibility(View.INVISIBLE);
                            moveObjectAnimator(handNoClick,50, 50, 80, 78, 21, 2000);
                            break;
                        case 21:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,80, 78, 80, 78, 22, 600);
                            break;
                        case 22:
                            handClick.setVisibility(View.INVISIBLE);
                            setDelayedText(getText(R.string.manual_info_text11).toString(),300);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_10);
                            moveObjectAnimator(handNoClick,50, 50, 75, 17, 23, 2000);
                            break;
                        case 23:
                            handClick.setVisibility(View.VISIBLE);
                            moveObjectAnimator(handClick,75, 17, 75, 17, 24, 600);
                            break;

                        case 24:
                            handClick.setVisibility(View.INVISIBLE);
                            handNoClick.setVisibility(View.INVISIBLE);
                            setDelayedText(getText(R.string.manual_info_text12).toString(),300);
                            manualBackgroundView.setImageResource(R.drawable.manual_info_11);
                            moveObjectAnimator(handNoClick,50, 50, 75, 17, 25, 3000);
                            playPause.setVisibility(View.INVISIBLE);
                            byeByeSignal();
                            break;

                        case 25:
                            //saveSharedPrefs(MyGlobals.SHOW_MANUAL_BY_EVERY_START,"yes");
                            getFragmentManager().beginTransaction().replace(R.id.fragment_container, new ChooseBoardFragment()).commit();
                            break;
                    }



                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

    }

    private void byeByeSignal() {
        manualLayoutFinish.setVisibility(View.VISIBLE);
        manualLayoutFinish.setBackground(getResources().getDrawable(R.drawable.button_border_new_record, null));
        textManualFinish.setVisibility(View.VISIBLE);
        textManualFinish.setText(getText(R.string.manual_info_text12).toString());

        YoYo.with(Techniques.Flash)
                .duration(1500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                })
                .playOn(manualLayoutFinish);
    }

    private void setDelayedText(String text, int time){
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                textManual.setText(text);
            }
        }, time);
    }

    public int scrWidthInPixels() {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) Objects.requireNonNull(getActivity()).getApplicationContext().getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public int scrHeightInPixels() {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) Objects.requireNonNull(getActivity()).getApplicationContext().getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public void saveSharedPrefs(String key, String value){
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(MyGlobals.SHARED_PREFS_POOL, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
