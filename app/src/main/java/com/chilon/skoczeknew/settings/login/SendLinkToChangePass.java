package com.chilon.skoczeknew.settings.login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.chilon.skoczeknew.MyGlobals;
import com.chilon.skoczeknew.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class SendLinkToChangePass extends Fragment {

    TextView textView;
    EditText enterEmail;
    Button buttonSendLink;
    ProgressBar progressBar;
    FirebaseAuth auth;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_pass, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textView = view.findViewById(R.id.forg_pass_text);
        enterEmail = view.findViewById(R.id.forg_pass_enter_email);
        buttonSendLink = view.findViewById(R.id.forg_pass_button_send);
        progressBar = view.findViewById(R.id.forg_pass_progress_bar);

        progressBar.setVisibility(View.INVISIBLE);


        buttonSendLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!enterEmail.getText().toString().isEmpty()) {
                    if (MyGlobals.isEmailValid(enterEmail.getText().toString())){
                        progressBar.setVisibility(View.VISIBLE);
                        auth.sendPasswordResetEmail(enterEmail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    enterEmail.setText("");
                                    textView.setText(R.string.send_pass_text_link_sent);
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getContext(), "" + getResources().getString(R.string.invalid_email), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Email CAN NOT be empty.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
