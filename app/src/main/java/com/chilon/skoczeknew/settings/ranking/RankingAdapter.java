package com.chilon.skoczeknew.settings.ranking;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chilon.skoczeknew.MyGlobals;
import com.chilon.skoczeknew.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class RankingAdapter extends RecyclerView.Adapter<RankingAdapter.RankingViewHolder> {
    private ArrayList<RankingItem> rankingList;
    private LayoutInflater mInflater;

    public RankingAdapter(Context context, ArrayList<RankingItem> rankingList) {
        this.mInflater = LayoutInflater.from(context);
        this.rankingList = rankingList;
    }

    @NonNull
    @Override
    public RankingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_ranking_list, parent, false);
        return new RankingViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RankingViewHolder holder, int position) {
        RankingItem currentItem = rankingList.get(position);
        holder.place.setText(String.valueOf(currentItem.getPosition()));
        holder.image.setImageResource(currentItem.getUserImage());
        holder.userName.setText(currentItem.getUserName());
        holder.score.setText(String.valueOf(currentItem.getScore()));
        holder.city.setText(currentItem.getCity());
        holder.country.setText(currentItem.getCountry());

        //highlight my user in ranking list
        if (currentItem.getUserName().toLowerCase().equals(loadSharedPrefs(MyGlobals.USER_NAME_KEY, holder).toLowerCase())) {
            holder.itemView.setBackgroundColor(Color.parseColor("#9ddef6"));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#e6e6fa"));
        }
        //add medal image for three first places
        if (currentItem.getPosition() == 1) {
            holder.place.setBackgroundResource(R.drawable.med_gold);
            holder.place.setText("");
        } else if (currentItem.getPosition() == 2) {
            holder.place.setBackgroundResource(R.drawable.med_silver);
            holder.place.setText("");
        } else if (currentItem.getPosition() == 3) {
            holder.place.setBackgroundResource(R.drawable.med_bronze);
            holder.place.setText("");
        } else {
            holder.place.setBackgroundResource(0);
        }
    }

    private String loadSharedPrefs(String key, RankingViewHolder holder) {
        SharedPreferences sharedPreferences = holder.image.getContext().getSharedPreferences(MyGlobals.SHARED_PREFS_POOL, MODE_PRIVATE);
        return sharedPreferences.getString(key, "0");
    }

    @Override
    public int getItemCount() {
        return rankingList.size();
    }

    public static class RankingViewHolder extends RecyclerView.ViewHolder {
        TextView place;
        ImageView image;
        TextView userName;
        TextView score;
        TextView city;
        TextView country;

        RankingViewHolder(View itemView) {
            super(itemView);
            place = itemView.findViewById(R.id.id_position);
            image = itemView.findViewById(R.id.id_user_image);
            userName = itemView.findViewById(R.id.id_user_name);
            score = itemView.findViewById(R.id.id_ranking_score);
            city = itemView.findViewById(R.id.id_city);
            country = itemView.findViewById(R.id.id_label_country);
        }
    }
}
